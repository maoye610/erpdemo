package com.yanxj.erp.admin.system.security.jwt;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * 自定义的 User 对象 用户安全校验除了密码和权限其他信息塞到token 用于日志记录
 * @author you
 * @date 2019/4/15
 */

@Data
@Accessors(chain = true)
public class JwtUser implements UserDetails {
    private static final long serialVersionUID = -5477864747092434199L;
    /**
     * 用户id
     */
    private Long id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 登录账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    public JwtUser(Long id, String name, String username, String password  ,Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;

        this.authorities = authorities;

    }

    /**
     * 权限
     */
    private Collection<? extends GrantedAuthority> authorities;

    public JwtUser(String username, String password, Collection<? extends GrantedAuthority> grantedAuthorities) {
        this.username = username;
        this.password = password;
        this.authorities = grantedAuthorities;
    }
    public JwtUser(){

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
