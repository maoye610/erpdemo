package com.yanxj.erp.admin.util;

import com.baomidou.mybatisplus.extension.exceptions.ApiException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;

/**
 * @author you
 * @date 2019/3/29 13:45
 */
@Api(tags = "异常处理,支持返回所有http请求方式")
@Slf4j
@ControllerAdvice
public class ExceptionController {
    @Value("${jwt.header}")
    private String head;
//    @Resource
//    private MyJwtTokenUtil myJwtTokenUtil;

    @ApiOperation("全部异常处理返回200")
    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public Result error(Exception e, HttpServletRequest request, HttpServletResponse response) {
        e.printStackTrace();
        String token = request.getHeader(head);
//        if (StringUtils.isEmpty(token) || null == myJwtTokenUtil.getClaimsFromToken(token)) {
//            log.error("未知错误 " + "message:" + e.getMessage(), e.toString());
//        } else {
//            log.error("未知错误 " + "id: " + myJwtTokenUtil.getUserIdFromHttpRequest(request) + "token: " + request.getHeader(head) + "message:" + e.getMessage(), e.toString());
//        }
        response.setStatus(200);
        return ResultGenerator.genFailResult("服务器繁忙");
    }

    @ApiOperation("自定义异常处理返回6000")
    @ExceptionHandler(value = {ApiException.class})
    @ResponseBody
    public Result apiError(ApiException e, HttpServletResponse response) {
        e.printStackTrace();
        log.error("自定义异常错误 " + e.getMessage(), e.toString());
        response.setStatus(200);
        return ResultGenerator.genFailResult(e.getMessage());
    }

    @ApiOperation("用户密码验证失败")
    @ExceptionHandler(value = {InternalAuthenticationServiceException.class})
    @ResponseBody
    public Result badCredentials2(Exception e, HttpServletResponse response) {
        e.printStackTrace();
        response.setStatus(200);
        return ResultGenerator.genFailResult(e.getMessage());
    }

    @ApiOperation("用户密码验证失败")
    @ExceptionHandler(value = {BadCredentialsException.class})
    @ResponseBody
    public Result badCredentials(Exception e, HttpServletResponse response) {
        e.printStackTrace();
        response.setStatus(200);
        return ResultGenerator.genFailResult("账户不存在或密码有误");
    }

    @ApiOperation("没有登录账户")
    @ExceptionHandler(value = {UsernameNotFoundException.class})
    @ResponseBody
    public Result accountNotFound(Exception e, HttpServletResponse response) {
        e.printStackTrace();
        response.setStatus(200);
        return ResultGenerator.genFailResult("账户不存在");
    }

    @ApiOperation("找不到文件")
    @ExceptionHandler(value = {FileNotFoundException.class})
    @ResponseBody
    public Result fileNotFound(Exception e, HttpServletResponse response) {
        e.printStackTrace();
        response.setStatus(404);
        return ResultGenerator.genNotFoundError();
    }

    @ApiOperation("请求方法不允许")
    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    @ResponseBody
    public Result methodNotSupported(Exception e, HttpServletResponse response) {
        e.printStackTrace();
        response.setStatus(404);
        return ResultGenerator.genNotFoundError();
    }
}
