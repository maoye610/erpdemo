package com.yanxj.erp.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author yanxj
 * @date 2020-09-10
 */
@EnableAsync
@SpringBootApplication(scanBasePackages = {
        "com.yanxj.erp.admin"
},exclude = DataSourceAutoConfiguration.class)
@MapperScan("com.yanxj.erp.admin.basics.mapper")
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
