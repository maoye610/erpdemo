package com.yanxj.erp.admin.system.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token过滤
 *
 * @author you
 * @date 2019/1/7 13:37
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Value("${jwt.header}")
    private String head;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Value("${jwt.redisHead}")
    private String redisHead;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        String token = request.getHeader(this.head);
        if (token == null || !token.startsWith(tokenHead)) {
            chain.doFilter(request, response);
            return;
        }
        final String authToken = token.substring(tokenHead.length());
        String username = jwtTokenUtil.getUsernameFromToken(authToken);
        // 判断redis有没有这个token
        if (false) {
            request.getRequestDispatcher("/401").forward(request, response);
            return;
        }
        // 帐号密码登录
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            // 这里我们要重新去数据库中查询用户的权限
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
            // 判断token是否有效和失效
            if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                // 这个来授权建立上下文对象
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        // SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(null, null, null));
        chain.doFilter(request, response);
    }
}
