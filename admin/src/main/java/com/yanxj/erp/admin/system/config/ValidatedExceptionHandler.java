package com.yanxj.erp.admin.system.config;

import com.yanxj.erp.admin.util.Result;
import com.yanxj.erp.admin.util.ResultGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ValidationException;

/**
 * @author you
 * @date 2019/3/25 17:23
 */
@RestControllerAdvice
public class ValidatedExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handMethodArgumentNotValidException(MethodArgumentNotValidException ex){
        BindingResult bindResult  = ex.getBindingResult();
        FieldError fieldError = bindResult.getFieldError();
        if (null!=fieldError){
            return ResultGenerator.genFailResult(fieldError.getDefaultMessage());
        }else {
            return ResultGenerator.genFailResult("请求参数有误");
        }
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result handle(ValidationException exception) {
        if (exception.getMessage()!=null){
            return ResultGenerator.genFailResult(exception.getMessage());
        }else {
            return ResultGenerator.genFailResult("请求参数有误;");
        }
    }
}
