package com.yanxj.erp.admin.system.controller;

import cn.hutool.json.JSONObject;
import com.yanxj.erp.admin.basics.model.dto.ComAdminDo;
import com.yanxj.erp.admin.system.model.AuthData;
import com.yanxj.erp.admin.system.security.AuthService;
import com.yanxj.erp.admin.system.security.jwt.JwtTokenUtil;
import com.yanxj.erp.admin.system.security.model.RoleVO;
import com.yanxj.erp.admin.system.vo.LoginReqVO;
import com.yanxj.erp.admin.util.Result;
import com.yanxj.erp.admin.util.ResultGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author yanxj
 * @date 2020/9/10 14:45
 */
@RestController
public class LoginController {

    @Value("${jwt.header}")
    private String headerAndToken;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Value("${jwt.expiration}")
    private Long expiration;

    @Resource
    private AuthService authService;

    @PostMapping(value = "/sys/login")
    public Result createAuthenticationToken(@RequestBody LoginReqVO loginDTO) throws AuthenticationException {
        String username = loginDTO.getUsername();
        String password = loginDTO.getPassword();
        String token = authService.login(username, password);
        Map<String, Object> data = new HashMap<>(2);
        data.put("token", "Bearer " + token);
        data.put("expiration", new Date(System.currentTimeMillis() + expiration).getTime());
        return ResultGenerator.genSuccessResult(data);
    }

    @GetMapping("/index")
    public Result index(HttpServletRequest request) {
        return ResultGenerator.genSuccessResult();
    }


    @PostMapping(value = "/sys/logout")
    public Result logout(HttpServletRequest request) {
        String token = request.getHeader(headerAndToken);
        return ResultGenerator.genSuccessResult("登出成功");
    }
}
