package com.yanxj.erp.admin.system.config;


import com.yanxj.erp.admin.system.security.handle.MyAccessDeniedHandler;
import com.yanxj.erp.admin.system.security.handle.MyPointUnauthorizedHandler;
import com.yanxj.erp.admin.system.security.jwt.JwtAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author you
 * @date 2019/3/16 11:19
 */
@Configurable
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    @Autowired
    public WebSecurityConfig(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    /**
     * 注入自定义filter
     *
     * @return com.jade.crm.security.JwtAuthenticationTokenFilter
     * @author you
     * @date 2019/1/7 14:43
     */
    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() {
        return new JwtAuthenticationTokenFilter();
    }

    /**
     * 配置authenticationManagerBuilder
     * @author you
     * @date 2019/3/30 11:09
     * @param authenticationManagerBuilder 身份认证校验方式
     */
    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                // 设置UserDetailsService
                .userDetailsService(this.userDetailsService)
                // 使用BCrypt进行密码的hash
                .passwordEncoder(passwordEncoder());
    }

    /**
     * 配置注入
     *
     * @return org.springframework.security.authentication.AuthenticationManager
     * @author you
     * @date 2019/1/7 18:30
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    /**
     * 装载BCrypt密码编码器
     *  14是设置密码匹配时间 约2-3秒
     * @return org.springframework.security.crypto.password.PasswordEncoder
     * @author you
     * @date 2019/1/7 14:47
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(14);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //关闭csrf
        http.csrf().disable()
                //用了token 这里去掉session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                //允许OPTIONS的跨域请求
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.GET, "/**/*.jpg", "/**/*.png", "/swagger-ui.html", "/webjars/**",
                        "/image/**", "/v2/api-docs/**", "/swagger-resources/**", "/configuration/**").permitAll()
                //对于获取token的api 要匿名授权
                .antMatchers(HttpMethod.POST,"/sys/**").permitAll()
                .antMatchers(HttpMethod.GET,"/sys/**").permitAll()

                .antMatchers(HttpMethod.GET,"/img/**","/accept/**").permitAll()
                .antMatchers(HttpMethod.POST,"/img/**").permitAll()
                .antMatchers(HttpMethod.GET,"/swagger-ui.html").permitAll()
                //管理员可以访问所有接口
//                .antMatchers("/**").permitAll()
                .antMatchers("/accept/**").hasAuthority("accept")
                //开始测试接口
                .antMatchers("/**/test/**").permitAll()
                //开启头像获取接口
                .antMatchers("/avatar").permitAll()
                //对于其他请求都要验证
                .anyRequest().authenticated();
        // 静止缓存
        http.headers().cacheControl();

        //添加filter过滤和判断token
        http.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

        //配置自定义403
        http.exceptionHandling()
                .accessDeniedHandler(new MyAccessDeniedHandler())
                .authenticationEntryPoint(new MyPointUnauthorizedHandler());
    }
}
