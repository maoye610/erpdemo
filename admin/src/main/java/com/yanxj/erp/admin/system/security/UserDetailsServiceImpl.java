package com.yanxj.erp.admin.system.security;
import com.yanxj.erp.admin.basics.model.dto.ComAdminDo;
import com.yanxj.erp.admin.system.model.AuthData;
import com.yanxj.erp.admin.system.security.jwt.JwtUser;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
/**
 * 用户登录获取权限 登录账号 昵称 id
 *
 * @author yanxj
 * @date 2020/9/10
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        ComAdminDo adminDO = new ComAdminDo();
        adminDO.setName("yanxj");
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        adminDO.setPassword(encoder.encode("123456"));
        if (null == adminDO || 0 == adminDO.getName().length() || !adminDO.getName().equals(s)) {
            throw new UsernameNotFoundException("找不到用户: " + s);
        } else {
            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
            return new JwtUser(adminDO.getId(),
                        adminDO.getName(), adminDO.getUserName(), adminDO.getPassword(),
                     authorities);
        }
    }
}
