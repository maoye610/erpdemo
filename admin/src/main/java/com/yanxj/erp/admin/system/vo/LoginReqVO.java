package com.yanxj.erp.admin.system.vo;

import com.yanxj.erp.admin.util.RegexpConstants;
import com.yanxj.erp.admin.util.RegexpMessageConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@ApiModel("登录数据")
public class LoginReqVO {

    @ApiModelProperty(dataType = "string",value = "用户名",required = true,example = "username")
    @NotBlank(message = "用户名不能为空")
    @Pattern(regexp = RegexpConstants.LOGIN_USERNAME,message = RegexpMessageConstants.LOGIN_USERNAME)
    private String username;

    @ApiModelProperty(dataType = "string",value = "密码",required = true,example = "password")
    @NotNull(message = "密码不能为空")
    @Pattern(regexp = RegexpConstants.PASSWORD,message = RegexpMessageConstants.PASSWORD)
    private String password;
}