package com.yanxj.erp.admin.system.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * jwt相关工具包
 *
 * @author you
 * @date 2019/1/7 11:21
 */
@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;
    /**
     * 密钥
     */
    private final String secret = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKo++i9J9dzAFtbxwowKDCo2mxi7MXxE8A8VvssaydWjjgmEz/HHMPLOhi1182a1si4pWL0/MizKnquD7T2Bu4jpQbAFnkNYEMEyq/kw904Xl0JCQHYFuvnI99RE8Q3KlTP6kEUGDjV34EL6vBGJcQvArLtj1xoP8y0nIfJ2Pw5TAgMBAAECgYAGGB8IllMwxceLhjf6n1l0IWRH7FuHIUieoZ6k0p6rASHSgWiYNRMxfecbtX8zDAoG0QAWNi7rn40ygpR5gS1fWDAKhmnhKgQIT6wW0VmD4hraaeyP78iy8BLhlvblri2nCPIhDH5+l96v7D47ZZi3ZSOzcj89s1eS/k7/N4peEQJBAPEtGGJY+lBoCxQMhGyzuzDmgcS1Un1ZE2pt+XNCVl2b+T8fxWJH3tRRR8wOY5uvtPiK1HM/IjT0T5qwQeH8Yk0CQQC0tcv3d/bDb7bOe9QzUFDQkUSpTdPWAgMX2OVPxjdq3Sls9oA5+fGNYEy0OgyqTjde0b4iRzlD1O0OhLqPSUMfAkEAh5FIvqezdRU2/PsYSR4yoAdCdLdT+h/jGRVefhqQ/6eYUJJkWp15tTFHQX3pIe9/s6IeT/XyHYAjaxmevxAmlQJBAKSdhvQjf9KAjZKDEsa7vyJ/coCXuQUWSCMNHbcR5aGfXgE4e45UtUoIE1eKGcd6AM6LWhx3rR6xdFDpb9je8BkCQB0SpevGfOQkMk5i8xkEt9eeYP0fi8nv6eOUcK96EXbzs4jV2SAoQJ9oJegPtPROHbhIvVUmNQTbuP10Yjg59+8=";

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.tokenHead}")
    private String tokenHeader;

    @Value("${jwt.header}")
    private String tokenName;

    /**
     * 从数据声明说生产token
     *
     * @param claims 数据声明
     * @return java.lang.String
     * @author you
     * @date 2019/1/7 11:37$2a$10$3PSFPXzrktLOEgCeblFTIO.TuXLL7ef8CwHC.5bGpX59LhztubLhe
     */
    private String generateToken(Map<String, Object> claims) {
        // 这里设置token失效时间 目前设置一周
        Date expirationDate = new Date(System.currentTimeMillis() + expiration * 1000);
        // 生成签名密钥
        SecretKey key = Keys.hmacShaKeyFor(secret.getBytes());
        return Jwts.builder().setClaims(claims).signWith(key).setExpiration(expirationDate).compact();
    }

    /**
     * 通过token获取数据说明 如果获取过程出错则返回null
     *
     * @param token
     * @return io.jsonwebtoken.Claims
     * @author you
     * @date 2019/1/7 11:42
     */
    public Claims getClaimsFromToken(String token) {
        if (token.startsWith(tokenHeader)) {
            token = token.substring(tokenHeader.length());
        }
        Claims claims;
        try {
            //生成签名密钥
            SecretKey key = Keys.hmacShaKeyFor(secret.getBytes());
            claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            System.err.println("getClaimsFromToken err: " + e.getMessage()  );
            e.printStackTrace();
            claims = null;
        }
        return claims;
    }

    /**
     * 通过request获取token中保存的信息
     *
     * @param request
     * @return io.jsonwebtoken.Claims
     * @author you
     * @date 2019/4/16 18:41
     */
    public Claims getClaimsFromRequest(HttpServletRequest request) {
        String token = request.getHeader(tokenName);
        return getClaimsFromToken(token);
    }

    /**
     * 通过用户名生产token
     *
     * @param jwtUser jwt信息封装类
     * @return java.lang.String
     * @author you
     * @date 2019/1/7 11:46
     */
    public String generateToken(JwtUser jwtUser) {
        Map<String, Object> claims = new HashMap<>(4);
        claims.put("sub", jwtUser.getUsername());
        claims.put("id", jwtUser.getId() + "");
        claims.put("name", jwtUser.getName());
        claims.put("created", new Date());
        return generateToken(claims);
    }

    /**
     * 通过token获取相应的参数
     *
     * @param token token
     * @param key
     * @return java.lang.String
     * @author you
     * @date 2019/4/15 15:36
     */
    private String getClaimFromToken(String token, String key) {

        if (token.startsWith(tokenHeader)) {
            token = token.substring(tokenHeader.length());
        }

        Claims claims = getClaimsFromToken(token);
        Date expiration = claims.getExpiration();
        if (expiration.before(new Date())) {
            return null;
        }
        return String.valueOf(claims.get(key));
    }

    /**
     * 通过token获取用户名
     *
     * @param token token
     * @return java.lang.String
     * @author you
     * @date 2019/1/7 11:51
     */
    public String getUsernameFromToken(String token) {

        if (token.startsWith(tokenHeader)) {
            token = token.substring(tokenHeader.length());
        }

        String username;
        try {
            Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    /**
     * 通过token获取用户id
     *
     * @param token token
     * @return java.lang.Long
     * @author you
     * @date 2019/3/27 15:47
     */
    public Long getUserIdFromToken(String token) {
        String value = getClaimFromToken(token, "id");
        if (null != value) {
            return Long.parseLong(value);
        } else {
            return null;
        }
    }

    /**
     * 获取站点
     * @param token
     * @return
     */
    public Long getSiteIdFromToken(String token) {
        String value = getClaimFromToken(token, "siteId");
        if (null != value) {
            return Long.parseLong(value);
        } else {
            return null;
        }
    }

    /**
     * 通过HttpServletRequest获取用户id
     *
     * @param request req
     * @return java.lang.Long
     * @author you
     * @date 2019/3/27 16:14
     */
    public Long getUserIdFromHttpRequest(HttpServletRequest request) {

        String token = request.getHeader(tokenName);

        return getUserIdFromToken(token);
    }

    /**
     * 通过HttpServletRequest获取用户名(登录账号)
     *
     * @param request req
     * @return java.lang.String
     * @author you
     * @date 2019/4/16 17:31
     */
    public String getUsernameFromRequest(HttpServletRequest request) {
        String token = request.getHeader(tokenName);
        return getUsernameFromToken(token);
    }

    /**
     * 验证令牌是否已经过期
     *
     * @param token
     * @return java.lang.Boolean
     * @author you
     * @date 2019/1/7 12:55
     */
    public Boolean isTokenExpired(String token) {
        try {
            Claims claims = getClaimsFromToken(token);
            Date expiration = claims.getExpiration();
            return expiration.before(new Date());
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 刷新token
     *
     * @param token token
     * @return java.lang.String
     * @author you
     * @date 2019/1/7 11:53
     */
    public String refreshToken(String token) {
        String refreshToken;
        try {
            Claims claims = getClaimsFromToken(token);
            claims.put("createTime", new Date());
            refreshToken = generateToken(claims);
        } catch (Exception e) {
            refreshToken = null;
        }
        return refreshToken;
    }

    /**
     * 验证token
     *
     * @param token       token
     * @param userDetails 用户参数
     * @return boolean
     * @author you
     * @date 2019/1/7 13:00
     */
    public boolean validateToken(String token, UserDetails userDetails) {
        JwtUser user = (JwtUser) userDetails;
        String username = getUsernameFromToken(token);
        return username.equals(user.getUsername()) && !isTokenExpired(token);
    }

    /**
     * 从Header中获取token
     */
    public String getTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader(tokenName);
        if (StringUtils.isEmpty(token) || isTokenExpired(token)) {
            return null;
        }
        return token;
    }
}
