package com.yanxj.erp.admin.basics.model.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;


/**
 *
 * @author yanxj
 * @since
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ComAdminDo {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(notes = "类型(普通=1,管理=2)")
    private Integer type;

    @ApiModelProperty(notes = "用户名")
    private String userName;

    @ApiModelProperty(notes = "密码")
    private String password;

    @ApiModelProperty(notes = "头像")
    private String avatar;

    @ApiModelProperty(notes = "姓名")
    private String name;

    @ApiModelProperty(notes = "活动状态(激活=1,禁止=2)")
    private Integer activity;

    @ApiModelProperty(notes = "性别(男=1,女=2,保密=3)")
    private Integer sex;

    @ApiModelProperty(notes = "联系电话")
    private String phone;

    @ApiModelProperty(notes = "邮箱")
    private String email;

    @ApiModelProperty(notes = "证件类型(身份证=1)")
    private String cardType;

    @ApiModelProperty(notes = "证件编号")
    private String cardNo;

    @ApiModelProperty(notes = "职称")
    private String jobTitle;

    @ApiModelProperty(notes = "手机号")
    private String mobile;

    @ApiModelProperty(notes = "办公室")
    private String officeAddr;

    @ApiModelProperty(notes = "办公电话")
    private String officePhone;

    @ApiModelProperty(notes = "分机号")
    private String officePhoneExt;

    @ApiModelProperty(notes = "虚拟网")
    private String virtualnum;

    @ApiModelProperty(notes = "授权数据(JSON)")
    private String authData;

    @ApiModelProperty(notes = "组织结构id")
    private Long orgId;

    @ApiModelProperty(notes = "唯一标识")
    private String uuid;

}