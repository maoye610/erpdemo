package com.yanxj.erp.admin.system.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class AuthData {

    /**
     * 功能 - 权限
     */
    List<Long> power;
    /**
     * 角色
     */
    List<Long> role;
    /**
     * 部门
     */
    List<Long> department;
    /**
     * 组织结构
     */
    List<Long> organization;
    /**
     * 岗位
     */
    List<Long> position;
}
