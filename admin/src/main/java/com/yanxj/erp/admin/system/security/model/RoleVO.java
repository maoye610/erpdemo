package com.yanxj.erp.admin.system.security.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

@Data
public class RoleVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 上级部门
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    private String name;

    /**
     * 活动状态(激活=1，禁止=2)
     */
    private Integer activity;

    /**
     * 上级部门
     */
    private String description;

    /**
     * 图标
     */
    private String icon;

    /**
     * 授权数据(JSON)
     */
    private String authData;


    /**
     * 排序(降序)
     */
    private Integer sort;

    /**
     * 组织id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orgId;

    private List<RoleVO> children;
}
