package com.yanxj.erp.admin.util;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ResultGenerator {
    private static final String DEFAULT_SUCCESS_MESSAGE = "操作成功";

    public static Result getResult(Boolean isSuccess, String failText) {
        return getResult(null, isSuccess, DEFAULT_SUCCESS_MESSAGE, failText);
    }

    public static Result getResult(Boolean isSuccess, String successText, String failText) {
        return getResult(null, isSuccess, successText, failText);
    }

    public static Result getResult(Object data, Boolean isSuccess, String successText, String failText) {
        Result result = new Result();
        if (isSuccess) {
            result.setCode(ResultCodeEnum.SUCCESS);
            result.setMessage(successText);
        } else {
            result.setCode(ResultCodeEnum.FAIL);
            result.setMessage(failText);
        }
        if (data != null) {
            result.setData(data);
        }
        return result;
    }

    public static Result genSuccessResult() {
        return new Result()
                .setCode(ResultCodeEnum.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE);
    }

    public static Result genSuccessResult(Object data) {
        return new Result()
                .setCode(ResultCodeEnum.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genSuccessResult(Object data, String message) {
        return new Result()
                .setCode(ResultCodeEnum.SUCCESS)
                .setMessage(message)
                .setData(data);
    }

    public static Result genSuccessMsg(String message) {
        return new Result().setCode(ResultCodeEnum.SUCCESS).setMessage(message);
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(ResultCodeEnum.FAIL)
                .setMessage(message);
    }

    public static Result genFailResult() {
        return new Result()
                .setCode(ResultCodeEnum.FAIL)
                .setMessage("请求失败");
    }

    public static Result genUnauthorizedResult() {
        return new Result()
                .setCode(ResultCodeEnum.UNAUTHORIZED)
                .setMessage("权限不足！");
    }

    public static Result genTokenErrorResult() {
        return new Result()
                .setCode(ResultCodeEnum.TOKEN_FAIL)
                .setMessage("认证失败");
    }

    public static Result genServerError() {
        return new Result()
                .setCode(ResultCodeEnum.INTERNAL_SERVER_ERROR)
                .setMessage("服务器未知错误");
    }

    public static Result genServerError(String message) {
        return new Result().setCode(ResultCodeEnum.INTERNAL_SERVER_ERROR)
                .setMessage(message);
    }

    public static Result genNotFoundError() {
        return new Result()
                .setCode(ResultCodeEnum.NOT_FOUND)
                .setMessage("找不到文件或者页面");
    }

    public static Map<String, Object> generateResultMap(List list, Long total) {
        return new HashMap<String, Object>(2) {
            {
                put("list", list);
                put("total", total);
            }
        };
    }
}